import { OptionInfo } from "./option-info";

/** 機能クラス */
export interface Feature {
    /** 機能種別 */
    code?:number;
    
    /** 表示名 */
    display?:string;

    /** 値 */
    value?:string;

    /** 値リスト */
    valueList?:{[val:string]:string};

    /** 選択可能一覧 */
    options?:OptionInfo[];

    /** 選択タイプ */
    type?:"toggle" | "select";
}
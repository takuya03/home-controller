import { Feature } from "./feature";

/**
 * デバイス情報
 */
export interface Device {
    /** 識別ID */
    id?:number;
    
    /** 表示名 */
    display?:string;

    /**
     *  種別
     * @example 温水器
     */
    kind?:string;

    /** 設置部屋 */
    room?:string;

    /** 機能一覧 */
    features?:Feature[];
}
/** 選択情報 */
export interface OptionInfo{
    /** 表示名 */
    display?:string;

    /** 値 */
    value?:string;
    
    /** 重要な値であるか(主に停止に使用する) */
    isImportant?:boolean;
}
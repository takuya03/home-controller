/**
 * 機能情報ベースクラス
 */
export class FeatureData {
    /** 表示名 */
    display?:string;
    /** 状態 */
    status?:string;
    /** 値リスト */
    valueList?:{[val:string]:string}
}

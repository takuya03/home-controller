import { Component, OnInit } from '@angular/core';
import { Feature } from 'home-controller-common';
import { FeatureData } from './feature-data';
import { SelectData } from './select-data';
import { SwitchData } from './switch-data';

/**
 * 機能一覧コンポーネント
 */
@Component({
  selector: 'app-feature-list',
  templateUrl: './feature-list.component.html',
  styleUrls: ['./feature-list.component.scss']
})
export class FeatureListComponent implements OnInit {
  private sampleData:Feature[] = [
    {
      display : "test1",
      value : "40",
      valueList : {
        "40" : "Off",
        "41" : "On"
      },
      options : [
        {
          display : "Off",
          value : "40",
          isImportant : true
        },
        {
          display : "On",
          value : "41"
        }
      ],
      type: "toggle"
    },
    {
      display : "test2",
      value : "40",
      valueList : {
        "40" : "Stop",
        "41" : "Play"
      },
      options : [
        {
          display : "Stop",
          value : "40",
          isImportant : true
        },
        {
          display : "Play",
          value : "41"
        }
      ],
      type : "select"
    },
    {
      display : "test3",
      value : "80",
      valueList : {
        "80" : "sel1",
        "81" : "sel2",
        "82" : "sel3"
      },
      options : [
        {
          display : "sel1",
          value : "80"
        },
        {
          display : "sel2",
          value : "81"
        },
        {
          display : "sel3",
          value : "82"
        }
      ],
      type : "select"
    }
  ];


  public features:FeatureData[] = [];

  constructor() { }

  ngOnInit(): void {
    this.sampleData.forEach(element => {
      this.addData(element);
    });
  }

  private addData(d:Feature) {
    switch (d.type) {
      case 'select':
        let sd = new SelectData();
        sd.display = d.display;
        sd.valueList = d.valueList;
        sd.value = d.value;
        if (d.options) {
          var after = [];
          d.options.forEach(element => {
            if (element.isImportant) {
              after.push(element);
            }
            else {
              sd.addCommand(element);
            }
          });
          after.forEach(e => {
            sd.addCommand(e);
          });
        }
        this.features.push(sd);
        break;

      case 'toggle':
        let swd = new SwitchData();
        swd.display = d.display;
        swd.valueList = d.valueList;
        swd.options = d.options;
        swd.updateValue(d.value);
        this.features.push(swd);
        break;
    }
  }

}

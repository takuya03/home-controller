import { OptionInfo } from 'home-controller-common';
import { ICommand } from '../feature/feature.component';
import { FeatureData } from './feature-data';

/**
 * 選択式のデータ
 */
export class SelectData extends FeatureData {
    /** コマンド情報リスト */
    commands:ICommand[] = [];

    /**
     * コマンドの追加
     * @param opt 選択可能情報
     */
    public addCommand(opt:OptionInfo) {
        let _this = this;
        var cmd:ICommand = {
            display: opt.display,
            command: () => {
                console.log(opt.value);
                _this.status = opt.display;
            }
        };
        if (opt.isImportant) {
            cmd.color = "warn";
        }
        this.commands.push(cmd);
    }

    private _value:string;

    /**
     * 値の設定
     */
    public set value(v : string) {
        if (this._value === v) return;
        this._value = v;
        this.status = this.valueList[v];
    }
}

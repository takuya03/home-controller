import { OptionInfo } from 'home-controller-common';
import { FeatureData } from './feature-data';

/**
 * On/Offスイッチデータ
 */
export class SwitchData extends FeatureData {
    private _checked:boolean;
    /** チェック状態 */
    public get isChecked() : boolean {
      return this._checked;
    }
    
    public set isChecked(v : boolean) {
      if (this._checked === v) return;
      this._checked = v;
      if (this.valueList) {
        let idx = v ? 1 : 0;
        this.status = this.valueList[Object.keys(this.valueList)[idx]];
        console.log(`On/Off switch. ${this.options[idx]}`);
      }
    }
  
    /** 設定可能情報リスト */
    options?:OptionInfo[];
  
    public updateValue(val:string) {
      if (!this.valueList) {
        this.isChecked = false;
      }
      else if (Object.keys(this.valueList)[0] === val) {
        this.isChecked = false;
      } else {
        this.isChecked = true;
      }
    }
}

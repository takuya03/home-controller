import { Component, OnInit } from '@angular/core';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  public data;

  public status:string = "Off";

  private _isCheck:boolean;

  
  public get isCheck() : boolean {
    return this._isCheck;
  }
  
  public set isCheck(v : boolean) {
    this._isCheck = v;
    this.status = v ? "On" : "Off";
  }
  


  constructor(private homeservice:HomeService) { }

  ngOnInit(): void {
  }

  test() {
    this.homeservice.getFacilities().subscribe(obj => {
      this.data = obj;
    }, err => {
      console.error(err);
    });
  }
}

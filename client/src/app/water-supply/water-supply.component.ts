import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICommand } from '../feature/feature.component';

@Component({
  selector: 'app-water-supply',
  templateUrl: './water-supply.component.html',
  styleUrls: ['./water-supply.component.scss']
})
export class WaterSupplyComponent implements OnInit {

  public bathStatus:string = "不明";

  public bathCommands:ICommand[] = [
    {
      display:"湯張り",
      command:this.filling
    },
    {
      display:"停止",
      command:this.stop,
      color:"warn"
    }
  ];

  constructor(private client:HttpClient) { }

  ngOnInit(): void {
    this.refresh();
  }

  update(res) {
    if (res.value) {
      switch (res.value) {
        case '41':
          this.bathStatus = '湯張り中';
          break;
        case '42':
          this.bathStatus = '停止中';
          break;
        case '43':
          this.bathStatus = '保温中';
          break;
        default:
          this.bathStatus = `不明(${res.value}`;
          break;
      }
    }
    else {
      alert("想定外の形式です。")
    }
  }

  /** 状態の更新 */
  refresh() {
    this.client.get("api/echonet-lite/water-supply-test/bath").subscribe(res => {
      this.update(res);
    }, err => {
      alert("情報の取得に失敗しました。" + JSON.stringify(err))
    });
  }

  /** 湯張りの実施 */
  filling() {
    this.client.put("api/echonet-lite/water-supply-test/bath", {value:'41'}).subscribe(res => {
      this.update(res);
    }, err => {
      alert("情報の取得に失敗しました。" + JSON.stringify(err))
    });
  }

  /** 湯張りの停止 */
  stop() {
    this.client.put("api/echonet-lite/water-supply-test/bath", {value:'42'}).subscribe(res => {
      this.update(res);
    }, err => {
      alert("情報の取得に失敗しました。" + JSON.stringify(err))
    });
  }
}

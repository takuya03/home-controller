import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private client:HttpClient) { }

  getFacilities() {
    return this.client.get('api/echonet-lite/facilities');
  }
}

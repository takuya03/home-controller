import { Component, Input, OnInit } from '@angular/core';

export interface ICommand {
  display:string;
  command:Function;
  color?:string;
}

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.scss']
})
export class FeatureComponent implements OnInit {
  /** 表示名 */
  @Input() display:string;
  /** 状態 */
  @Input() status:string;
  /** コマンドリスト */
  @Input() commands:ICommand;

  constructor() { }

  ngOnInit(): void {
  }

}

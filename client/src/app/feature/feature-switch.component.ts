import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-feature-switch',
  templateUrl: './feature-switch.component.html',
  styleUrls: ['./feature-switch.component.scss']
})
export class FeatureSwitchComponent implements OnInit {
  /** 表示名 */
  @Input() display:string;

  /** 状態 */
  @Input() status:string;

  /** チェック状態の変更通知 */
  @Output() isCheckedChange = new EventEmitter<boolean>();

  /** 内部変数 */
  private _checked:boolean = false;

  /** チェック状態 */
  @Input() public get isChecked() : boolean {
    return this._checked;
  }

  /** チェック状態 */
  public set isChecked(v : boolean) {
    if (this._checked === v) return;
    this._checked = v;
    this.isCheckedChange.emit(v);
  }

  public changedToggle(arg:MatSlideToggleChange) {
    this.isChecked = arg.checked;
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}

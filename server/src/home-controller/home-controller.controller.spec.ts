import { Test, TestingModule } from '@nestjs/testing';
import { HomeControllerController } from './home-controller.controller';

describe('HomeControllerController', () => {
  let controller: HomeControllerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HomeControllerController],
    }).compile();

    controller = module.get<HomeControllerController>(HomeControllerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

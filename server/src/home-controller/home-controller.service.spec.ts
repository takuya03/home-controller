import { Test, TestingModule } from '@nestjs/testing';
import { HomeControllerService } from './home-controller.service';

describe('HomeControllerService', () => {
  let service: HomeControllerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HomeControllerService],
    }).compile();

    service = module.get<HomeControllerService>(HomeControllerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { Injectable } from '@nestjs/common';
import { Device } from 'home-controller-common';
import { ElDictionary } from 'eldoc/el-dictionary';
import { ElDictionaryService, ELObject, FacilitiesService } from '@hc/echonet-lite';
import { HomeFeatureService } from '@hc/home-feature-lib';
import { throws } from 'assert';

@Injectable()
export class HomeControllerService {
    private elDictionary:ElDictionary;

    constructor(
        private facilites:FacilitiesService,
        private featuresService:HomeFeatureService,
        private elDictionaryService:ElDictionaryService) {
            this.elDictionary = elDictionaryService.getELDictionary();
    }

    /**
     * デバイスリストを取得
     */
    getDeviceList() : Device[] {
        var res:Device[] = [];
        Object.values(this.facilites.getObjectList()).forEach(element => {
            var dev = this.parseDevice(element);
            if (dev) {
                res.push(dev);
            }
        });
        return res;
    }

    /**
     * デバイスの詳細を取得
     * @param id 
     */
    getDeviceInfo(id:number) {
        return this.facilites.getObject(id);
    }

    /**
     * デバイスの機能一覧を取得
     * @param id 
     */
    getDeviceFeatures(id:number) {
        var obj = this.facilites.getObject(id);
        return this.featuresService.getFeatures(obj);
    }

    /**
     * デバイス機能の値を設定
     * @param did デバイスID
     * @param fcode 機能コード
     * @param val 設定値
     */
    setFeatureValue(did:number, fcode:number, val:string) {
        var obj = this.facilites.getObject(did);
        return this.featuresService.searchService(obj).setValue(obj, fcode, val);
    }

    /**
     * ELObjectをDeviceに変換
     * @param obj 
     */
    private parseDevice(obj:ELObject) {
        if (!obj.code) {
            console.error("code does not exist in ELObject.");
            console.debug(JSON.stringify(obj));
        }
        if (obj.code.substr(0, 4) === '0ef0') {
            // プロファイルオブジェクトの場合はundefinedを返す
            return undefined;
        }
        var info:Device = {
            id: obj.id,
            display: obj.display
        };
        // 種類情報
        if (this.elDictionary && this.elDictionary.object[obj.code.substr(0, 4)] && this.elDictionary.object[obj.code.substr(0, 4)].display) {
            info.kind = this.elDictionary.object[obj.code.substr(0, 4)].display;
        } else {
            info.kind = obj.code;
        }
        // 機能情報
        info.features = this.featuresService.getFeatures(obj);
        return info;
    }
}

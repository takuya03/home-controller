import { Body, Controller, Get, Param, Put, Query } from '@nestjs/common';
import { HomeControllerService } from './home-controller.service';

@Controller('api/v1/home-controller')
export class HomeControllerController {
    constructor(private service:HomeControllerService){}

    @Get('devices')
    getDevices() {
        return this.service.getDeviceList();
    }

    @Get('devices/:did')
    getDevice(
        @Param('did') did:number
    ){
        return this.service.getDeviceInfo(Number(did));
    }

    @Get('devices/:did/features')
    getDeviceFeatures(
        @Param('did') did:number
    ) {
        return this.service.getDeviceFeatures(Number(did));
    }

    @Put('devices/:did/features/:fcode')
    putDeviceFraturesValue(
        @Param('did') did:number,
        @Param('fcode') fcode:number,
        @Body('value') val:string
    ){
        return this.service.setFeatureValue(Number(did), Number(fcode), val);
    }
}

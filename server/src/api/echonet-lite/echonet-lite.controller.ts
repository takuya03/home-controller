import { FacilitiesService } from '@hc/echonet-lite';
import { Controller, Get, Param } from '@nestjs/common';
import { EchonetObject } from '../interfaces/echonet-object';

@Controller('api/echonet-lite')
export class EchonetLiteController {
    constructor(
        private readonly facilitiesService:FacilitiesService) {
    }

    /**
     * 施設リストを取得
     */
    @Get("facilities")
    onFacilities() {
        var res:{[address:string]:{[code:string]:EchonetObject}} = {}
        var facilities = this.facilitiesService.getFacilities();
        var address = Object.keys(facilities);
        address.forEach(add => {
            var node:{[code:string]:EchonetObject} = {};
            var codes = Object.keys(facilities[add]);
            codes.forEach(code => {
                node[code] = {};
            });
            res[add] = node;
        });
        return res;
    }

    /**
     * ECHONETオブジェクトリストを取得
     */
    @Get("objects")
    getObjects() {
    }
}

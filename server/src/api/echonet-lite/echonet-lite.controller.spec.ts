import { Test, TestingModule } from '@nestjs/testing';
import { EchonetLiteController } from './echonet-lite.controller';

describe('EchonetLiteController', () => {
  let controller: EchonetLiteController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EchonetLiteController],
    }).compile();

    controller = module.get<EchonetLiteController>(EchonetLiteController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

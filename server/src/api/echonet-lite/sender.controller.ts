import { ElCommunicationService } from '@hc/echonet-lite';
import { Controller, Get, Query, Param, Put } from '@nestjs/common';

@Controller('api/echonet-lite/sender')
export class SenderController {
    constructor(private readonly elcservice:ElCommunicationService) {
    }

    @Get(':address/:obj/:epc')
    onGet(
        @Param('address') address:string,
        @Param('obj') obj:string,
        @Param('epc') epc:string) {
        if (!obj.match(/[0-9a-fA-F]{6}/g)) {
            return {err:"`obj` format error."}
        }
        if (!epc.match(/[0-9a-fA-F]{2}/g)) {
            return {err:"`epc` format error." }
        }
        return this.elcservice.getProperties(address, "0ef001", obj, [epc], 5000);
    }

    @Put(':address/:obj/:epc')
    onPut(
        @Param('address') address:string,
        @Param('obj') obj:string,
        @Param('epc') epc:string,
        @Query('value') val:string) {
        if (!obj.match(/[0-9a-fA-F]{6}/g)) {
            return {err:"`obj` format error."}
        }
        if (!epc.match(/[0-9a-fA-F]{2}/g)) {
            return {err:"`epc` format error." }
        }
        return this.elcservice.setProperty(address, "0ef001", obj, epc, val, 5000);
    }
}

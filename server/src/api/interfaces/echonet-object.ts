export interface EchonetObject {
    /** id */
    id?:number;
    /** 機器ID */
    machineid?:number;
    /** 表示名 */
    display?:string;
    /** 状態 */
    status?:boolean;
}
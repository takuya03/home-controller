import { FacilitiesService } from '@hc/echonet-lite';
import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ShutdownService } from './shutdown/shutdown.service';

@Controller()
export class AppController {
  constructor(
    private readonly shutdownService: ShutdownService,
    private readonly appService: AppService,
    private readonly elfacilitiesService: FacilitiesService) {
    }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get("shutdown")
  onShutdown() {
    this.shutdownService.shutdown();
    return "hoge";
  }

  @Get("facilities")
  onGetFacilities() {
    return this.elfacilitiesService.getFacilities();
  }

  @Get("facilities/renew")
  onGetFacilitiesRenew() {
    this.elfacilitiesService.renewFacilities();
    return this.elfacilitiesService.getFacilities();
  }
}

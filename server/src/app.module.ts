import { Module, OnModuleDestroy, OnApplicationShutdown, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ShutdownService } from './shutdown/shutdown.service';
import { EchonetLiteController } from './api/echonet-lite/echonet-lite.controller';
import { SenderController } from './api/echonet-lite/sender.controller';
import { HomeControllerController } from './home-controller/home-controller.controller';
import { HomeControllerService } from './home-controller/home-controller.service';
import { EchonetLiteModule } from '@hc/echonet-lite';
import { DefaultOperationStatusService, HomeFeatureService } from '@hc/home-feature-lib';
import { WaterSupplyService } from '@hc/home-feature-lib/water-supply.service';
import { SensorCorpusService } from './sensor-corpus/sensor-corpus.service';

@Module({
  imports: [EchonetLiteModule, HttpModule],
  controllers: [AppController, EchonetLiteController, SenderController, HomeControllerController],
  providers: [
    AppService,
     ShutdownService,
     HomeControllerService,
     HomeFeatureService,
     SensorCorpusService,

     // 以下、機能サービス
     WaterSupplyService,
     DefaultOperationStatusService],
})
export class AppModule {
}

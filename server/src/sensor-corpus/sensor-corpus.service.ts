import { ElCommunicationService, FacilitiesService } from '@hc/echonet-lite';
import { HttpService, Injectable, OnModuleDestroy, OnModuleInit } from '@nestjs/common';
import { SecureConfiguration } from 'src/secure';
import { URLSearchParams } from 'url';

@Injectable()
export class SensorCorpusService implements OnModuleInit, OnModuleDestroy {
    private _intervalHandle:any;

    constructor(
        private _facilities:FacilitiesService,
        private _elService:ElCommunicationService,
        private _httpService:HttpService) {}

    onModuleInit() {
        this._intervalHandle = setInterval(() => {
            this.onUploadData();
        }, 1 * 60 * 1000);  // 10分ごと
    }
    onModuleDestroy() {
        clearInterval(this._intervalHandle);
    }

    onUploadData() {
        let cnt = 0;
        const objects = this._facilities.getObjectList();
        Object.values(objects).forEach(elm => {
            if (elm.code && elm.address && (elm.code.substr(0, 4) === "0279")) {   // 太陽光発電の場合
                ++cnt;
                const no = cnt;
                this._elService.getProperties(elm.address, this._elService.defaultDEOJ, elm.code, ['e0'], 5000).then(res => {
                    const now = new Date();
                    const nowFormatedTime = `${now.getFullYear()}-${String(now.getMonth()+1).padStart(2, '0')}-${String(now.getDate()).padStart(2, '0')} ${String(now.getHours()).padStart(2, '0')}:${String(now.getMinutes()).padStart(2, '0')}:${String(now.getSeconds()).padStart(2, '0')}.${String(now.getMilliseconds()).padStart(3, '0')}`;
                    const csvText = `${SecureConfiguration.SensorCorpus.sensorPreFix}${no},${nowFormatedTime},#,${parseInt(res['e0'], 16)}`;
                    
                    const params = new URLSearchParams();
                    params.append('session',SecureConfiguration.SensorCorpus.sessionCode);
                    params.append('csv',csvText);

                    try {
                        this._httpService.post(
                            SecureConfiguration.SensorCorpus.baseUrl + "/data/store",
                            params,
                            {
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }).subscribe(e => {
                                ;
                            });
                    } catch(err) {
                        console.warn(`Upload error: ${err}`);
                    }
                }).catch(e => {
                    console.warn("timeout.");
                });
            }
        })
    }
}

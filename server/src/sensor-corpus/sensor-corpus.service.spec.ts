import { Test, TestingModule } from '@nestjs/testing';
import { SensorCorpusService } from './sensor-corpus.service';

describe('SensorCorpusService', () => {
  let service: SensorCorpusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SensorCorpusService],
    }).compile();

    service = module.get<SensorCorpusService>(SensorCorpusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

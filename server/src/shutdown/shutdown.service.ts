import { Injectable } from '@nestjs/common';
import { Subject } from 'rxjs';

@Injectable()
export class ShutdownService {
    /**
     * シャットダウンリスナー
     */
    private shutdownListener$: Subject<void> = new Subject();
    
    /**
     * 
     * @param shutdownFn シャットダウンイベント
     */
    subscribeToShutdown(func: () => void): void {
        this.shutdownListener$.subscribe(() => func());
    }
    
    /**
     * シャットダウンの実施
     */
    shutdown() {
        this.shutdownListener$.next();
    }
}

/** 選択可能リスト用値情報 */
export interface ELDSelectable {
    /** 表示名 */
    display?:string;
}

/** EPC情報 */
export interface ELDEpc{
    /** 表示名 */
    display?:string;
    /** 値タイプ */
    type?: "signed char" | "unsigned char" | "signed short" | "unsigned short" | "signed long" | "unsigned long";
    /** 値のサイズ */
    length?:number | number[];
    /** 選択可能リスト */
    selectable?:{[val:string]:ELDSelectable}
}

/** オブジェクト情報 */
export interface ELDObject {
    /** 表示名 */
    display?:string;
    /** EPC情報 */
    EPC?:{[code:string]:ELDEpc}
}

/** 辞書情報定義 */
export interface ElDictionary {
    /** ECHONETオブジェクト情報 */
    object?:{[code:string]:ELDObject}
    /** 共有情報 */
    common?:{
        /** 機器オブジェクトのスーパークラス */
        machine?:{
            /** EPC情報 */
            EPC?:{[code:string]:ELDEpc};
        }
    }
}
import { ElCommunicationService, ELObject } from '@hc/echonet-lite';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { Feature } from 'home-controller-common';
import { IFeatureService } from './feature.service';
import { HomeFeatureService } from './home-feature.service';

@Injectable()
export class WaterSupplyService implements IFeatureService, OnModuleInit {
    private readonly autoStatusCode = 0;

    constructor(
        private featureService:HomeFeatureService,
        private elService:ElCommunicationService
    ){
    }

    onModuleInit() {
        // モジュールの読み込み時に登録
        this.featureService.registerFeatureService(this);
    }

    isMatchObject(obj: ELObject): boolean {
        // 温水器クラス
        return obj.code && (obj.code.substr(0, 4) === "026b");
    }
    getFeatures(obj: ELObject): Feature[] {
        // 温水器オブジェクト
        var list:Feature[] = [];
        if ((obj.getters && obj.getters.find(e => e === 'ea') && obj.setters && obj.setters.find(e => e === 'e3'))) {
            list.push({
                code: this.autoStatusCode,
                display: "状態",
                type: 'select',
                value: obj.properties['ea'].value,
                valueList: {
                    "41": "湯張り中",
                    "42": "停止中",
                    "43": "保温中"
                },
                options: [
                    {
                        display: "湯張り",
                        value: "41"
                    },
                    {
                        display: "停止",
                        value: "42",
                        isImportant: true
                    }
                ]
            });
        }
        return list;
    }
    setValue(obj: ELObject, code: number, value: string): Promise<string> | undefined {
        switch (code) {
            case this.autoStatusCode:
                return this.elService.setProperty(obj.address, this.elService.defaultDEOJ, obj.code, "e3", value);
            default:
                return undefined;
        }
    }
}

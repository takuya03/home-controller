import { Test, TestingModule } from '@nestjs/testing';
import { WaterSupplyService } from './water-supply.service';

describe('WaterSupplyService', () => {
  let service: WaterSupplyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WaterSupplyService],
    }).compile();

    service = module.get<WaterSupplyService>(WaterSupplyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

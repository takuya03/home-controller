import { ElCommunicationService, ELObject } from '@hc/echonet-lite';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { Feature } from 'home-controller-common';
import { IFeatureService } from './feature.service';
import { HomeFeatureService } from './home-feature.service';

@Injectable()
export class DefaultOperationStatusService implements IFeatureService, OnModuleInit {
    private readonly statusCode:number = 0;

    constructor(
        private featureService:HomeFeatureService,
        private elService:ElCommunicationService
    ) {
    }

    onModuleInit() {
        // モジュールの読み込み時に登録
        this.featureService.registerFeatureService(this);
    }

    isMatchObject(obj:ELObject) {
        return true;
    }

    getFeatures(obj: ELObject): Feature[] {
        if (obj.getters && obj.getters.find(e => e === '80') && obj.setters && obj.setters.find(e => e === '80')) {
            return [
                {
                    code: this.statusCode,
                    display: "動作状態",
                    type: "toggle",
                    value: obj.properties['80']?.value,
                    valueList: {
                        '30': 'On',
                        '31': 'Off'
                    },
                    options: [
                        {
                            display: "Off",
                            value: "31",
                            isImportant: true
                        },
                        {
                            display: "On",
                            value: "30"
                        }
                    ]
                }
            ];
        }
    }

    setValue(obj: ELObject, code: number, value: string) {
        switch (code) {
            case this.statusCode:
                return this.elService.setProperty(obj.address, this.elService.defaultDEOJ, obj.code, "80", value, 5000);
            default:
                return undefined;
        }
    }
}

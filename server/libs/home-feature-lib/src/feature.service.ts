import { ELObject } from "@hc/echonet-lite";
import { Feature } from "home-controller-common";

/**
 * 機能サービスインターフェース
 */
export interface IFeatureService {
    /**
     * このサービスが一致するサービスであるかをチェックする
     * @param obj ECHONET Object
     */
    isMatchObject(obj:ELObject):boolean;

    /**
     * ECHONET Objectから機能一覧を取得する
     * @param obj ECHONET Object
     */
    getFeatures(obj:ELObject):Feature[];

    /**
     * 値を変更
     * @param code Featureの識別コード
     * @param value 設定する値
     */
    setValue(obj:ELObject, code:number, value:string):Promise<string> | undefined;
}

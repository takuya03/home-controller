import { ELObject } from '@hc/echonet-lite';
import { Injectable } from '@nestjs/common';
import { IFeatureService } from './feature.service';

@Injectable()
export class HomeFeatureService {
    /**
     * 機能サービス一覧
     */
    private services:IFeatureService[] = [];

    /**
     * ECHONET OBJECT毎のサービス情報
     */
    private objectServices:{[id:number]:IFeatureService} = {};

    constructor() { }

    /**
     * サービスの登録
     * @param service 
     */
    registerFeatureService(service:IFeatureService) {
        this.services.push(service);
    }

    /**
     * 機能リストを取得する
     * @param obj ECHONET Object
     */
    getFeatures(obj:ELObject) {
        if (this.objectServices[obj.id]) {
            return this.objectServices[obj.id].getFeatures(obj);
        } else {
            var service = this.searchService(obj);
            this.objectServices[obj.id] = service;
            return service.getFeatures(obj);
        }
    }

    /**
     * 一致する機能サービスを取得
     * @param obj ECHONET OBJECT
     */
    searchService(obj:ELObject) {
        for (let index = 0; index < this.services.length; index++) {
            const element = this.services[index];
            if (element.isMatchObject(obj)) return element;
        }
        return undefined;
    }
}

import { Test, TestingModule } from '@nestjs/testing';
import { HomeFeatureService } from './home-feature.service';

describe('HomeFeatureService', () => {
  let service: HomeFeatureService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HomeFeatureService],
    }).compile();

    service = module.get<HomeFeatureService>(HomeFeatureService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

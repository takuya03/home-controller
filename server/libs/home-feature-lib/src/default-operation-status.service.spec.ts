import { Test, TestingModule } from '@nestjs/testing';
import { DefaultOperationStatusService } from './default-operation-status.service';

describe('DefaultOperationStatusService', () => {
  let service: DefaultOperationStatusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DefaultOperationStatusService],
    }).compile();

    service = module.get<DefaultOperationStatusService>(DefaultOperationStatusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

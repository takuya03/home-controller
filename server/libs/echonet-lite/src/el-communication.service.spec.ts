import { Test, TestingModule } from '@nestjs/testing';
import { ElCommunicationService } from './el-communication.service';

describe('ElCommunicationService', () => {
  let service: ElCommunicationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ElCommunicationService],
    }).compile();

    service = module.get<ElCommunicationService>(ElCommunicationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

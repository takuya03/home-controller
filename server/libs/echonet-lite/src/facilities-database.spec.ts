import { FacilitiesDatabase } from './facilities-database';

describe('FacilitiesDatabase', () => {
  it('should be defined', () => {
    expect(new FacilitiesDatabase()).toBeDefined();
  });

  it('node id test', () => {
    var db = new FacilitiesDatabase();
    var node1 = db.getNodeFromResource("192.168.0.2");
    expect(node1.address).toEqual("192.168.0.2");
    expect(node1.id).toBeUndefined();

    var node2 = db.getNodeFromResource("192.168.0.3");
    expect(node2.address).toEqual("192.168.0.3");
    expect(node2.id).toBeUndefined();

    var id1 = db.setNode("192.168.0.2", "ABC");
    expect(node1.identity).toEqual("ABC");
    var node3 = db.getNode(id1);
    expect(node3.address).toEqual("192.168.0.2");
    
    var node4 = db.getNodeFromResource("192.168.0.2");
    node4.objects.set("0ef001", {id:1,properties:{}});

    expect(db.getNode(id1).objects.get("0ef001").id).toEqual(1);

    var id2 = db.setNode("192.168.0.3", "ABC");
    expect(id2).toEqual(id1);
    
    var id3 = db.setNode("192.168.0.5", "ABCD");
    expect(id3).toEqual(id2 + 1);
  });

  it('object id test', () => {
    var db = new FacilitiesDatabase();
    var obj1 = db.getObjectFromResource("192.168.0.2", "0ef001");
    expect(obj1.address).toEqual("192.168.0.2");
    expect(obj1.code).toEqual("0ef001");

    db.setNode(obj1.address, "ABC");
    expect(obj1.nodeIdentity).toEqual("ABC");
    expect(obj1.id).toEqual(0);

    var obj2 = db.getObjectFromResource("192.168.0.2", "0ef002");
    expect(obj2.id).toEqual(1);

    var obj3 = db.getObjectFromResource("192.168.0.3", "0ef001");
    expect(obj3.id).toBeUndefined();
  });
});

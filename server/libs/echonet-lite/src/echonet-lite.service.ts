import { Injectable, OnModuleInit, OnModuleDestroy } from '@nestjs/common';
import { ELSocket } from './elobj/elsockets';
import { RemoteInfo, Socket } from 'dgram';
import { ELData } from './elobj/eldata';
import { Subject, Subscribable, Subscription } from 'rxjs';
const el = require('echonet-lite');

interface receivedArgment {
    rinfo:RemoteInfo;
    els:ELData;
}

export interface OnReceivedEchonet {
    receivedEchonet(rinfo:RemoteInfo, els:ELData);
}

@Injectable()
export class EchonetLiteService implements OnModuleInit, OnModuleDestroy {
    /**
     * Echonet Lite ソケット
     */
    private sockets:ELSocket | Socket;

    /** 受信通知イベント */
    private receivedSubject = new Subject<receivedArgment>();

    private readonly objlist = ['05ff01']


    /**
     * モジュールの初期化時処理
     */
    onModuleInit() {
        this.sockets = el.initialize(this.objlist,
            (rinfo,els) => this.receivedELRequest(rinfo, els),
            0,
            {autoGetProperties: false});
        el.search();
    }

    /**
     * モジュールの削除時処理
     */
    onModuleDestroy() {
        if ('sock4' in this.sockets) {
            this.sockets.sock4.close();
            this.sockets.sock6.close();
        } else {
            this.sockets.close();
        }
    }

    /**
     * ECHONET Liteメッセージの受信を知らせるオブジェクトを登録
     * @param obj 通知先オブジェクト
     * @returns 
     */
    registerReceivedEchonet(obj:OnReceivedEchonet):Subscription {
        return this.receivedSubject.subscribe(val => obj.receivedEchonet(val.rinfo, val.els));
    }

    /**
     * ECHONET Lite情報の受信
     * @param rinfo アクセス元情報
     * @param els ECHONET Liteデータ
     */
    private receivedELRequest(rinfo:RemoteInfo, els:ELData) {
        this.receivedSubject.next({rinfo:rinfo, els:els});
    }
}

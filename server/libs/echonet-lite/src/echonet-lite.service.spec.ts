import { Test, TestingModule } from '@nestjs/testing';
import { EchonetLiteService } from './echonet-lite.service';

describe('EchonetLiteService', () => {
  let service: EchonetLiteService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EchonetLiteService],
    }).compile();

    service = module.get<EchonetLiteService>(EchonetLiteService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

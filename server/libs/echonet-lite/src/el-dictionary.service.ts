import { Injectable } from '@nestjs/common';
import { readFileSync } from 'fs';
import { ElDictionary } from 'eldoc/el-dictionary';

@Injectable()
export class ElDictionaryService {
    private filepath = './eldoc/el-dictionary-jp.json';
    
    private dict:ElDictionary;

    getELDictionary() {
        if (!this.dict) {
            this.dict = JSON.parse(readFileSync(this.filepath, 'utf8')) as ElDictionary;
        }
        return this.dict;
    }

    getELCodes() {
        return {
            profile: {
                identity: {
                    code: "83"
                },
                machinelist: {
                    code: "d6"
                },
                changedmachinelist: {
                    code: "d5"
                } 
            },
            machinesuper: {
                status: {
                    code: "80",
                    on: "30",
                    off: "31"
                },
                identity: {
                    code: "83"
                },
                infos: {
                    code: "9d"
                },
                setters: {
                    code: "9e"
                },
                getters: {
                    code: "9f"
                }
            }
        }
    }
}

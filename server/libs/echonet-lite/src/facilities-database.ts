import { writeFileSync, readFileSync, existsSync, mkdirSync } from "fs";
import { dirname } from "path";

export interface ELProperty {
    /** 値 */
    value?:any;
    /** タイムスタンプ */
    timestamp?:Date;
}

export interface ELObject {
    /** ID */
    id?:number;
    /** 識別情報 */
    identity?:string;
    /** 親ノードのIdentity */
    nodeIdentity?:string;
    /** IPアドレス */
    address?:string;
    /** ECHONETオブジェクトコード*/
    code?:string;
    /** 表示名 */
    display?:string;
    /** 
     * ECHONETオブジェクトリスト
     * ※プロファイルオブジェクトの場合のみ
     */
    objects?:string[];

    /** 取得可能リスト */
    getters?:string[];
    /** 設定可能リスト */
    setters?:string[];
    /** 通知プロパティリスト */
    infos?:string[];
    /** プロパティ情報リスト */
    properties:{[key:string]:ELProperty};
}

export interface ELNode {
    /** id */
    id?:number;
    /** Node識別子 */
    identity?:string;
    /** アドレス */
    address?:string;
    /** オブジェクトリスト */
    objects:Map<string,ELObject>;
}

export class FacilitiesDatabase {
    /** 最大NodeID */
    private currentNodeId:number = 0;
    /** 最大オブジェクトID */
    private currentObjectId:number = 0;

    /** IPアドレス主体のノードリスト */
    private addressNodeMap:Map<string, ELNode> = new Map<string,ELNode>();
    /** ID主体のノードリスト */
    private nodeIdMap:{[id:number]:ELNode} = {};

    /** ID主体のオブジェクトリスト */
    private objectIdMap:{[id:number]:ELObject} = {};

    /** アドレスマップのクリア */
    clear() {
        this.addressNodeMap.clear();
        var nodeIds = Object.keys(this.nodeIdMap);
        nodeIds.forEach(item => {
            var id = parseInt(item);
            var old = this.nodeIdMap[id];
            this.nodeIdMap[id] = {
                identity: old.identity,
                id: old.id,
                objects: new Map<string, ELObject>()
            };
        });

        var objIds = Object.keys(this.objectIdMap);
        objIds.forEach(item => {
            var id = parseInt(item);
            var old = this.objectIdMap[id];
            this.objectIdMap[id] = {
                nodeIdentity: old.nodeIdentity,
                code: old.code,
                id: old.id,
                properties:{}
            };
        })
    }

    getFacilities() { return this.addressNodeMap; }

    /**
     * ノード情報を取得
     * @param address 
     */
    getNodeFromResource(address:string) {
        if (this.addressNodeMap.has(address)) {
            return this.addressNodeMap.get(address);
        } else {
            var v:ELNode = {objects:new Map<string,ELObject>()}
            v.address = address;
            this.addressNodeMap.set(address, v);
            return v;
        }
    }
    
    /**
     * 指定したアドレスのノードが存在するか
     * @param address 
     */
    hasNodeFromResource(address:string) { return this.addressNodeMap.has(address);}

    /**
     * 削除
     * @param address 
     */
    removeNodeFromResource(address:string) {
        return this.addressNodeMap.delete(address);
    }

    /**
     * 
     * @param id 
     */
    getNode(id:number) {
        return this.nodeIdMap[id];
    }

    /**
     * ECHONETノード情報とIDを紐付け
     * @param address 
     * @param identity 
     */
    setNode(address:string, identity:string) : number {
        let isset = false;
        const keys = Object.keys(this.nodeIdMap);
        const node = this.getNodeFromResource(address);
        for (let idx = 0; idx < keys.length; idx++) {
            const keynum = parseInt(keys[idx]);
            if (this.nodeIdMap[keynum].identity === identity) {
                this.nodeIdMap[keynum] = node;
                node.id = keynum;
                isset = true;
            }
        }
        if (!isset) {
            const id = this.currentNodeId;
            this.currentNodeId++;
            node.id = id;
            node.identity = identity;
            this.nodeIdMap[id] = node;
        }
        node.objects.forEach((val) => {
            val.nodeIdentity = identity;
            this.setObject(val);
        });
        return node.id;
    }

    /**
     * リソース情報からECHONETオブジェクトを取得
     * @param address 
     * @param code 
     */
    getObjectFromResource(address:string, code:string) {
        const node = this.getNodeFromResource(address);
        if (!node.objects.has(code)) {
            let obj:ELObject = {properties:{}};
            obj.address = address;
            obj.code = code;
            obj.nodeIdentity = node.identity;
            node.objects.set(code, obj);
            this.setObject(obj)
        }
        return node.objects.get(code);
    }

    /**
     * ECHONETオブジェクトリストの取得
     */
    getObjectList() {
        return Object.values(this.objectIdMap);
    }

    /**
     * 指定したIDのECHONETオブジェクトを取得
     * @param id 
     */
    getObject(id:number) {
        return this.objectIdMap[id];
    }

    /**
     * データの保存
     * @param filepath 出力パス
     */
    save(filepath?:string){
        // 出力用IDマップ
        var nodeList:{[id:number]:ELNode} = {};
        var nodeIds = Object.keys(this.nodeIdMap);
        nodeIds.forEach(key => {
            var nid = parseInt(key);
            var item = this.nodeIdMap[nid];
            nodeList[nid] = {
                identity: item.identity,
                objects: undefined
            };
        });

        // 入力用IDマップ
        var objectList:{[id:number]:ELObject} = {};
        var objectIds = Object.keys(this.objectIdMap);
        objectIds.forEach(key => {
            var oid = parseInt(key);
            var item = this.objectIdMap[oid];
            objectList[oid] = {
                code: item.code,
                nodeIdentity: item.nodeIdentity,
                properties: undefined
            };
        });

        if (!filepath) {
            filepath = "./data/facilities.json";
        }
        if (!existsSync(dirname(filepath))){
            mkdirSync(dirname(filepath));
        }
        writeFileSync(filepath, JSON.stringify({nodes:nodeList, objects:objectList}));
    }

    /**
     * データの読み込み
     * @param filepath ファイルパス
     */
    load(filepath?:string) {
        
        if (!filepath) {
            filepath = "./data/facilities.json";
        }
        try {
            var data = JSON.parse(readFileSync(filepath, 'utf8'));

            this.addressNodeMap.clear();
            if ((data.nodes !== undefined) && (data.objects !== undefined)) {
                // nodeリスト
                var nids = Object.keys(data.nodes);
                var nidmap = {};
                var maxnid = 0;
                nids.forEach(key => {
                    var nid = parseInt(key);
                    var old = data.nodes[nid];
                    nidmap[nid] = {
                        identity: old.identity,
                        id: nid,
                        objects: new Map<string, ELObject>()
                    };
                    if (maxnid < nid) maxnid = nid;
                });
                
                // objectリスト
                var oids = Object.keys(data.objects);
                var oidmap = {};
                var maxoid = 0;
                oids.forEach(key => {
                    var oid = parseInt(key);
                    var old = data.objects[oid];
                    oidmap[oid] = {
                        nodeIdentity: old.nodeIdentity,
                        code: old.code,
                        id: oid,
                        properties:{}
                    };
                    if (maxoid < oid) maxoid = oid;
                });

                this.nodeIdMap = nidmap;
                this.currentNodeId = maxnid;
                this.objectIdMap = oidmap;
                this.currentObjectId = maxoid;
            } else {
                console.log("Not found database data.");
            }
        } catch (ex) {
            console.log("Not found database data." + JSON.stringify(ex));
        }
    }

    /**
     * ECHONETオブジェクトとIDリストを関連付けする
     * @param object 
     */
    private setObject(object:ELObject) {
        if (!object.nodeIdentity) return;   // node識別子の登録がない場合はキャンセル
        const keys = Object.keys(this.objectIdMap);
        for (let idx = 0; idx < keys.length; idx++) {
            const key = parseInt(keys[idx]);
            if ((this.objectIdMap[key].nodeIdentity === object.nodeIdentity) && (this.objectIdMap[key].code === object.code)) {
                this.objectIdMap[key] = object;
                object.id = key;
                return;
            }
        }
        var id = this.currentObjectId;
        this.currentObjectId++;
        this.objectIdMap[id] = object;
        object.id = id;
    }
}

import { Module, OnModuleInit } from '@nestjs/common';
import { EchonetLiteService, OnReceivedEchonet } from './echonet-lite.service';
import { RemoteInfo } from 'dgram';
import { ELData } from './elobj/eldata';
import { FacilitiesService } from './facilities.service';
import { ElCommunicationService } from './el-communication.service';
import { ElDictionaryService } from './el-dictionary.service';

class test implements OnReceivedEchonet{
  receivedEchonet(rinfo: RemoteInfo, els: ELData) {
    console.log(rinfo.address + ':' + rinfo.port);
  }

}

@Module({
  providers: [
    EchonetLiteService,
    ElDictionaryService,
    FacilitiesService,
    ElCommunicationService
  ],
  exports: [
    EchonetLiteService,
    ElDictionaryService,
    FacilitiesService,
    ElCommunicationService
  ]
})
export class EchonetLiteModule implements OnModuleInit {
  constructor(private elservice:EchonetLiteService) {}

  onModuleInit() {
    this.elservice.registerReceivedEchonet(new test());
  }

}

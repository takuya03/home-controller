export interface ELData {
    EHD:string;
    TID:string;
    SEOJ:string;
    DEOJ:string;
    EDATA:string;
    ESV:string;
    OPC:string;
    DETAIL:string;
    DETAILs;   
}
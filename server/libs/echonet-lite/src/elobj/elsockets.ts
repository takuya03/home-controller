import { Socket } from "dgram";

export interface ELSocket {
    sock4:Socket;
    sock6:Socket;
}
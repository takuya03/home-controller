import { Injectable } from '@nestjs/common';
import { OnReceivedEchonet, EchonetLiteService } from './echonet-lite.service';
import { RemoteInfo } from 'dgram';
import { ELData } from './elobj/eldata';
import { EventEmitter } from 'events';
import e from 'express';
import { FacilitiesService } from './facilities.service';
import { ElDictionaryService } from './el-dictionary.service';
const el = require('echonet-lite');

const receivedEmitter = "received";

@Injectable()
export class ElCommunicationService implements OnReceivedEchonet {
    /** wait時間 */
    private readonly waittime = 1000;
    /** Promiseオブジェクト用マップ */
    private map = new Map<any,Promise<any>>();
    
    /** イベントエミッター */
    private emitter = new EventEmitter();

    readonly defaultDEOJ = "0ef001";

    constructor(
        private readonly eldservice:ElDictionaryService,
        elservice:EchonetLiteService) {
        elservice.registerReceivedEchonet(this);
    }

    /**
     * 
     * @param key 
     * @param action 
     */
    private getPromise(key:any, action:Function) {
        var bp:Promise<void>;
        if (this.map.has(key)) {
            bp = this.map.get(key);
        }
        else {
            bp = Promise.resolve();
        }
        var ap = bp.then(_ => {
            action();
            // 連続での取得はデバイス側が対応できないので、waitする。
            return new Promise(resolve => {
                setTimeout(resolve, this.waittime);
            });
        });
        this.map.set(key, ap);
    }
    
    /**
     * 
     * @param address 
     * @param seoj 
     * @param deoj 
     * @param esv 
     * @param epc 
     * @param edt 
     */
    sendOPC1(address:string, seoj:string, deoj:string, esv:string | number, epc:string | number, edt?:string | number) {
        this.getPromise(address, () => {
            el.sendOPC1(address, seoj, deoj, esv, epc, edt);
        });
    }

    /**
     * 指定したEPC値を取得
     * @param address 
     * @param seoj 
     * @param deoj 
     * @param epcs
     * @param timeout タイムアウトまでの時間[ms]が設定されている場合はPromiseオブジェクトを返す 
     */
    getProperties(address:string, seoj:string, deoj:string, epcs:string[] | number[], timeout?:number) : Promise<{[epc:string]:string}> | undefined {
        var p = undefined;

        // byte値なのでEPCの数はとりあえず最大を255としておく(多分実際はもっと小さい)
        var epccnt = (epcs.length <= 255) ? epcs.length : 0xff;
        
        if (typeof timeout === 'number') {
            // タイムアウト設定がある場合は返却用のPromiseを作成する
            var data : {[epc:string]:string} = {};
            var _this = this;
            var emt = this.emitter;
            
            p = new Promise((resolve, reject) => {
                var handler = function(rinfo:RemoteInfo, els:ELData) {
                    if ((els.ESV === el.GET_RES) && (rinfo.address === address) && (els.SEOJ === deoj)) {
                        var keys = Object.keys(els.DETAILs);
                        keys.forEach(key => {
                            // GET_RESの情報からepcsの値を取得して保持
                            if (epcs.findIndex(e => ((typeof e === 'number') ? e.toString(16) : e) === key) != -1) {
                                data[key] = els.DETAILs[key];
                            }
                        });
                        if ((epcs.length !== 1) && (keys.length === 1) && (keys[0] === epcs[0])) {
                            // epcsに複数の指定があるにもかかわらず、先頭のプロパティかつ1つのプロパティ情報しか存在しない場合は
                            // デバイスが複数のプロパティ情報の一括取得に対応していないとして、1つづつ再送する
                            for (let i = 1; i < epcs.length; i++) {
                                _this.sendOPC1(address, seoj, deoj, el.GET, epcs[i]);
                            }
                        }
                        if (Object.keys(data).length === epccnt) {
                            // 全てのプロパティを受信完了
                            emt.off(receivedEmitter, handler);
                            resolve(data);
                        }
                    }
                }
                emt.on(receivedEmitter, handler);
                setTimeout(() => {
                    emt.off(receivedEmitter, handler);
                    // 取得できたところまでは返す
                    reject(data);
                }, timeout);
            });
        }
        this.getPromise(address, () => {
            try {
                var array = [
                    // ヘッダー
                    0x10,0x81,0x00,0x00,
                    parseInt(seoj.substr(0,2),16), parseInt(seoj.substr(2,2),16), parseInt(seoj.substr(4,2),16),
                    parseInt(deoj.substr(0,2),16), parseInt(deoj.substr(2,2),16), parseInt(deoj.substr(4,2),16),
                    parseInt(el.GET, 16),
                    epccnt
                ];
                for (let i = 0; i < epccnt; i++) {
                    const element = epcs[i];
                    if (typeof element === 'number') {
                        array.push(element);
                    } else {
                        array.push(parseInt(element, 16));
                    }
                    array.push(0x00);
                }
                const buffer = Buffer.from(array);
                el.sendBase(address, buffer);
            } catch (error) {
                console.error(error);
            }
        });
        return p;
    }

    /**
     * プロパティの設定
     * @param address 
     * @param seoj 
     * @param deoj 
     * @param epc 
     * @param timeout 
     */
    setProperty(address:string, seoj:string, deoj:string, epc:string | number, data:string | number, timeout?:number) : Promise<string> | undefined {
        var p = undefined;
        var esv = el.SETI
        if (typeof timeout === 'number') {
            // タイムアウト設定が行われている場合
            esv = el.SETC;
            var emt = this.emitter;
            
            p = new Promise((resolve, reject) => {
                var handler = function(rinfo:RemoteInfo, els:ELData) {
                    if ((els.ESV === el.SET_RES) && (rinfo.address === address) && (els.SEOJ === deoj) && (els.DETAILs[epc] !== undefined)) {
                        emt.off(receivedEmitter, handler);
                        resolve(els.DETAILs[epc]);
                    }
                    if ((els.ESV === el.SETC_SNA) && (rinfo.address === address) && (els.SEOJ === deoj) && (els.DETAILs[epc] !== undefined)) {
                        emt.off(receivedEmitter, handler);
                        reject("Response Not Possible");
                    }
                }
                emt.on(receivedEmitter, handler);
                setTimeout(() => {
                    emt.off(receivedEmitter, handler);
                    reject("timeout");
                }, timeout);
            });
        }
        this.sendOPC1(address, seoj, deoj, esv, epc, data);
        return p;
    }

    /**
     * プロパティリストを取得
     * @param address 宛先アドレス
     * @param obj 宛先のオブジェクトコード
     * @param timeout タイムアウト時間(未設定の場合はPromiseオブジェクトを返しません)
     * @returns timeoutが設定されている場合はPromiseオブジェクト
     */
    getPropertyList(address:string, obj:string, timeout?:number) : Promise<any> | undefined {
        var p = undefined;
        if (typeof timeout === 'number') {
            var emt = this.emitter;
            var infoprop = this.eldservice.getELCodes().machinesuper.infos.code;
            
            p = new Promise((resolve, reject) => {
                var handler = function(rinfo:RemoteInfo, els:ELData) {
                    if ((els.ESV === el.GET_RES) && (rinfo.address === address) && (els.SEOJ === obj) && (els.DETAILs[infoprop])) {
                        emt.off(receivedEmitter, handler);
                        resolve({address:address, obj:obj});
                    }
                }
                emt.on(receivedEmitter, handler);
                setTimeout(() => {
                    emt.off(receivedEmitter, handler);
                    reject({address:address, obj:obj});
                }, timeout);
            });
        }
        const seoj = [0x0e, 0xf0, 0x01];
        const deoj = el.toHexArray(obj);
        this.getPromise(address, () => {
            try {
                const buffer = Buffer.from([
                    // ヘッダー
                    0x10,0x81,0x00,0x00,
                    seoj[0], seoj[1], seoj[2],
                    deoj[0], deoj[1], deoj[2],
                    parseInt(el.GET, 16),
                    // EPCs
                    0x03,
                    parseInt(this.eldservice.getELCodes().machinesuper.getters.code, 16), 0x00,
                    parseInt(this.eldservice.getELCodes().machinesuper.setters.code, 16), 0x00,
                    parseInt(this.eldservice.getELCodes().machinesuper.infos.code, 16), 0x00
                ]);
                el.sendBase(address, buffer);
            } catch (error) {
                console.error(error);
            }
        });
        return p;
    }

    receivedEchonet(rinfo: RemoteInfo, els: ELData) {
        this.emitter.emit(receivedEmitter, rinfo, els);
    }
}

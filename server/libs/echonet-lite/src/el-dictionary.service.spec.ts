import { Test, TestingModule } from '@nestjs/testing';
import { ElDictionaryService } from './el-dictionary.service';

describe('ElDictionaryService', () => {
  let service: ElDictionaryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ElDictionaryService],
    }).compile();

    service = module.get<ElDictionaryService>(ElDictionaryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

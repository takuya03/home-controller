import { Injectable } from '@nestjs/common';
import { OnReceivedEchonet, EchonetLiteService } from './echonet-lite.service';
import { RemoteInfo } from 'dgram';
import { ELData } from './elobj/eldata';
import { ElCommunicationService } from './el-communication.service';
import { ElDictionaryService } from './el-dictionary.service';
import { FacilitiesDatabase, ELObject, ELNode } from './facilities-database';
const el = require('echonet-lite');

@Injectable()
export class FacilitiesService implements OnReceivedEchonet {
    /** 最後の更新日時 */
    private lastUpdate:Date = new Date();

    private db = new FacilitiesDatabase();

    constructor(
        private readonly eldservice:ElDictionaryService,
         private elcommunicationService:ElCommunicationService,
         elservice:EchonetLiteService) {
        elservice.registerReceivedEchonet(this);
        this.db.load();
    }

    /**
     * 施設情報の更新
     * ※実行後は30秒以上待機し、getFacilitiesメソッドで取得してください。
     */
    renewFacilities() {
        var now = new Date();
        var diff = now.getTime() - this.lastUpdate.getTime();
        if (diff >= 60000) { // 最後の更新から1分以上経過していた場合は更新する
            this.db.clear();
            this.lastUpdate = now;
            el.search();
        }
    }

    /**
     * ECHONETオブジェクトの取得
     * @param address IPアドレス
     * @param code ECHONET Liteオブジェクトコード
     */
    getObjectFromResource(address:string, code:string) {
        return this.db.getObjectFromResource(address, code);
    }

    /**
     * ECHONETノード情報の取得
     * @param address 
     */
    getNodeFromResource(address:string) {
        return this.db.getNodeFromResource(address);
    }

    /**
     * 施設リストを取得
     */
    getFacilities() : {[address:string]:{[code:string]:ELObject}} {
        var obj = {};
        this.db.getFacilities().forEach((val, key) => {
            var dev = {};
            val.objects.forEach((v, k) => {
                dev[k] = v;
            });
            obj[key] = dev;
        });
        return obj;
    }

    /**
     * ECHONET オブジェクトリストの取得
     */
    getObjectList() { return this.db.getObjectList(); }

    /**
     * 指定したIDのオブジェクトを取得
     * @param id 
     */
    getObject(id:number) { return this.db.getObject(id); }

    /**
     * プロパティリスト
     * @param address 
     * @param obj 
     * @param timeout 
     */
    getPropertyList(address:string, obj:string, timeout:number) {
        var p = this.elcommunicationService.getPropertyList(address, obj, timeout) as Promise<any>;
        return p.then((_) => {
            return this.getObjectFromResource(address, obj);
        });
    }

    /**
     * 
     * @param rinfo 
     * @param els 
     */
    receivedEchonet(rinfo: RemoteInfo, els: ELData) {
        if ((els.ESV === el.GET_RES) && (els.DEOJ.substr(0, 4) === '0ef0')) {
            // DEOJがノードプロファイルのみ処理
            var obj = this.getObjectFromResource(rinfo.address, els.SEOJ) as ELObject;
            
            if ((els.SEOJ.substr(0, 4) === '0ef0')  &&
            　(
                els.DETAILs[this.eldservice.getELCodes().profile.machinelist.code] ||
                els.DETAILs[this.eldservice.getELCodes().profile.changedmachinelist.code])) {
                // SEOJがノードプロファイルの場合はノード情報を取得するように変更
                // MEMO:d5は変更通知 d6は機器リスト

                // インスタンス情報を分解
                let detailStr = null;
                if (els.DETAILs[this.eldservice.getELCodes().profile.machinelist.code]) {
                    detailStr = els.DETAILs[this.eldservice.getELCodes().profile.machinelist.code];
                }
                else {
                    detailStr = els.DETAILs[this.eldservice.getELCodes().profile.changedmachinelist.code];
                }

                try {
                    // 分解した情報を元にプロパティ情報の取得を実行
                    if (detailStr) {
                        var array = [];
                        const objcnt = parseInt(detailStr.substr(0, 2), 16);
                        // オブジェクトリストの設定と状態の取得
                        for (let index = 0; index < objcnt; index++) {
                            const strobj = detailStr.substr(index * 6 + 2, 6);
                            array.push(strobj);
                            this.db.getObjectFromResource(rinfo.address, strobj);   // オブジェクト情報を作成
                            // 先に状態情報を取得する
                            this.elcommunicationService.sendOPC1(rinfo.address, this.elcommunicationService.defaultDEOJ, strobj, el.GET, this.eldservice.getELCodes().machinesuper.status.code);
                        }
                        obj.objects = array;
                        
                        // プロパティリストの取得
                        for (let index = 0; index < objcnt; index++) {
                            const strobj = detailStr.substr(index * 6 + 2, 6);
                            this.elcommunicationService.getPropertyList(rinfo.address, strobj);
                        }
                        // 識別子の取得
                        for (let index = 0; index < objcnt; index++) {
                            const strobj = detailStr.substr(index * 6 + 2, 6);
                            this.elcommunicationService.sendOPC1(rinfo.address, this.elcommunicationService.defaultDEOJ, strobj, el.GET, this.eldservice.getELCodes().machinesuper.identity.code);
                        }
                    }
                    // ノードプロファイルの識別子
                    el.sendOPC1(rinfo.address, this.elcommunicationService.defaultDEOJ, els.SEOJ, el.GET, this.eldservice.getELCodes().profile.identity.code);
                } catch(err) {
                    console.error(err);
                }
            }

            if ((els.SEOJ.substr(0, 4) === '0ef0') && els.DETAILs[this.eldservice.getELCodes().profile.identity.code]) {
                // 識別子プロパティ
                // ノードプロファイルの場合はdbに設定
                this.db.setNode(rinfo.address, els.DETAILs[this.eldservice.getELCodes().profile.identity.code]);
                obj.identity = els.DETAILs[this.eldservice.getELCodes().profile.identity.code];
                this.db.save();                
            }
            if (els.DETAILs[this.eldservice.getELCodes().machinesuper.identity.code]) {
                // 識別子プロパティ
                obj.identity = els.DETAILs[this.eldservice.getELCodes().machinesuper.identity.code];
            }
            if (els.DETAILs[this.eldservice.getELCodes().machinesuper.infos.code]) {
                // 詳細情報を分解
                const detailstr = els.DETAILs[this.eldservice.getELCodes().machinesuper.infos.code];
                const propcnt = parseInt(detailstr.substr(0, 2), 16);
                let array = [];
                for (let index = 0; index < propcnt; index++) {
                    array.push(detailstr.substr(2 * index + 2, 2));
                }
                obj.infos = array;
            }
            if (els.DETAILs[this.eldservice.getELCodes().machinesuper.setters.code]) {
                // 詳細情報を分解
                const detailstr = els.DETAILs[this.eldservice.getELCodes().machinesuper.setters.code];
                const propcnt = parseInt(detailstr.substr(0, 2), 16);
                let array = [];
                for (let index = 0; index < propcnt; index++) {
                    array.push(detailstr.substr(2 * index + 2, 2));
                }
                obj.setters = array;
            }
            if (els.DETAILs[this.eldservice.getELCodes().machinesuper.getters.code]) {
                // 詳細情報を分解
                const detailstr = els.DETAILs[this.eldservice.getELCodes().machinesuper.getters.code];
                const propcnt = parseInt(detailstr.substr(0, 2), 16);
                let array = [];
                for (let index = 0; index < propcnt; index++) {
                    array.push(detailstr.substr(2 * index + 2, 2));
                }
                obj.getters = array;
            }
            if (els.DETAILs[this.eldservice.getELCodes().machinesuper.getters.code]) {
                // getter,setter,infoの順に一度に取得している。
                // 複数個の取得に対応していないデバイスの場合はgetterしか情報が存在しないため、再度要求する
                if (!els.DETAILs[this.eldservice.getELCodes().machinesuper.setters.code]) {
                    this.elcommunicationService.sendOPC1(rinfo.address, this.elcommunicationService.defaultDEOJ, els.SEOJ, el.GET, this.eldservice.getELCodes().machinesuper.setters.code);
                }
                if (!els.DETAILs[this.eldservice.getELCodes().machinesuper.infos.code]) {
                    this.elcommunicationService.sendOPC1(rinfo.address, this.elcommunicationService.defaultDEOJ, els.SEOJ, el.GET, this.eldservice.getELCodes().machinesuper.infos.code);
                }
            }

            var keys = Object.keys(els.DETAILs);
            const tms = new Date();
            keys.forEach(key => {
                if ((key !== this.eldservice.getELCodes().profile.machinelist.code) &&
                    (key !== this.eldservice.getELCodes().profile.changedmachinelist.code) &&
                    (key !== this.eldservice.getELCodes().profile.identity.code) &&
                    (key !== this.eldservice.getELCodes().machinesuper.identity.code) &&
                    (key !== this.eldservice.getELCodes().machinesuper.infos.code) &&
                    (key !== this.eldservice.getELCodes().machinesuper.getters.code) &&
                    (key !== this.eldservice.getELCodes().machinesuper.setters.code)) {
                    // 上記で設定している、無視すべきプロパティ以外の情報をオブジェクトに設定
                    if (!obj.properties[key]) {
                        obj.properties[key] = {};
                    }
                    obj.properties[key].value = els.DETAILs[key];
                    obj.properties[key].timestamp = tms;
                }
            });
        }
    }

}
